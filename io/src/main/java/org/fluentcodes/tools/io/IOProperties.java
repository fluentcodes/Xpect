package org.fluentcodes.tools.io;

import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class IOProperties implements IO<Properties> {
    private final IOString ioString;

    public IOProperties() {
        this("application.properties", StandardCharsets.UTF_8);
    }

    public IOProperties(final String fileName) {
        this(fileName, StandardCharsets.UTF_8);
    }

    public IOProperties(final String fileName, final Charset encoding) {
        this.ioString = new IOString(fileName, encoding);
    }

    public String asString(Properties value) {
        return "";
    }

    public Properties asObject(String value) {
        return new Properties();
    }

    @Override
    public Properties read() {
        Properties properties = new Properties();
        try {
            properties.load(new StringReader(ioString.read()));
        } catch (Exception e) {
            throw new IORuntimeException("Problem loading '" + getFileName() + "'", e);
        }
        return properties;
    }

    @Override
    public String getFileName() {
        return ioString.getFileName();
    }

    @Override
    public void setFileName(final String fileName) {
        ioString.setFileName(fileName);
    }
}

