package org.fluentcodes.tools.io;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Read a String using {@link IOBytes}.
 */
public class IOString implements IO<String>, IOWrite<String>, IOStringInterface {
    private final IOBytes ioBytes;
    private final Charset encoding;

    /**
     * A constructor for StandardCharsets.UTF_8 without file name.
     */
    public IOString() {
        this(StandardCharsets.UTF_8);
    }

    /**
     * A constructor without file name.
     *
     * @param encoding the encoding different from StandardCharsets.UTF_8.
     */
    public IOString(Charset encoding) {
        super();
        this.ioBytes = new IOBytes();
        this.encoding = encoding;
    }

    /**
     * A constructor for StandardCharsets.UTF_8.
     *
     * @param fileName the file name to read/write.
     */
    public IOString(final String fileName) {
        this(fileName, StandardCharsets.UTF_8);
    }

    /**
     * A constructor for StandardCharsets.UTF_8.
     *
     * @param fileName the file name to read/write.
     * @param encoding the encoding different from StandardCharsets.UTF_8
     */
    public IOString(final String fileName, Charset encoding) {
        this.ioBytes = new IOBytes(fileName);
        this.encoding = encoding;
    }

    @Override
    public String asString(String value) {
        return value;
    }

    @Override
    public String asObject(String value) {
        return value;
    }

    @Override
    public void write(String content) {
        ioBytes.write(content.getBytes());
    }

    @Override
    public String read() {
        return asString(ioBytes.read());
    }

    @Override
    public Charset getEncoding() {
        return encoding;
    }

    @Override
    public String getFileName() {
        return ioBytes.getFileName();
    }

    @Override
    public void setFileName(final String fileName) {
        ioBytes.setFileName(fileName);
    }
}

