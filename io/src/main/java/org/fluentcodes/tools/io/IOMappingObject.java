package org.fluentcodes.tools.io;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Abstract class for serializing and deserializing objects.
 *
 * @param <T> the object to map
 */
public abstract class IOMappingObject<T> extends IOMappingAbstract<T> implements IO<T>, IOWrite<T> {
    private final IOString ioString;

    protected IOMappingObject(Class<?>... mappingClasses) {
        this(StandardCharsets.UTF_8, mappingClasses);
    }

    private IOMappingObject(Charset encoding, Class<?>... mappingClasses) {
        super(mappingClasses);
        this.ioString = new IOString(encoding);
    }

    protected IOMappingObject(final String fileName, Class<?>... mappingClasses) {
        this(fileName, StandardCharsets.UTF_8, mappingClasses);
    }

    private IOMappingObject(final String fileName, Charset encoding, Class<?>... mappingClasses) {
        super(mappingClasses);
        this.ioString = new IOString(fileName, encoding);
    }

    @Override
    public void write(T object) {
        ioString.write(asString(object));
    }

    @Override
    public T read() {
        return asObject(ioString.read());
    }

    @Override
    public String getFileName() {
        return ioString.getFileName();
    }

    @Override
    public void setFileName(final String fileName) {
        ioString.setFileName(fileName);
    }

}
