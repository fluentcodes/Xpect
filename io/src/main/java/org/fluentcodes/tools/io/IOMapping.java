package org.fluentcodes.tools.io;

/**
 * Basic methods for IO classes using objects serialization and deserialization.
 *
 * @param <T> the object type
 */
public interface IOMapping<T> extends IO<T> {

    Class<?>[] getMappingClasses();

    default boolean hasMappingClasses() {
        return getMappingClasses() != null && getMappingClasses().length > 0;
    }

    default Class<?> getMappingClass() {
        if (!hasMappingClasses()) {
            throw new IORuntimeException("Empty mapping class!");
        }
        return getMappingClasses()[0];
    }
}
