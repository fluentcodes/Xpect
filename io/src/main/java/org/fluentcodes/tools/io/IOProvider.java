package org.fluentcodes.tools.io;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IOProvider<T> {
    private static final Pattern aliasPattern = Pattern.compile("\\$\\{.*\\}");
    private final Map<String, IO<T>> providerMap;
    private final Map<String, String> aliases;
    private final Map<String, String> fileNames;

    public IOProvider(Collection<String> fileNames) {
        this.providerMap = new HashMap<>();
        this.aliases = new HashMap<>();
        this.fileNames = new HashMap<>();
        for (String fileName : fileNames) {
            String[] keyValue = fileName.split("=");
            if (keyValue.length == 1) {
                this.fileNames.put(fileName, fileName);
            } else if (keyValue.length == 2) {
                final String key = keyValue[0];
                final String value = keyValue[1];
                if (value.isEmpty() && key.isEmpty()) {
                    continue;
                }
                if (key.isEmpty()) {
                    this.fileNames.put(value, value);
                    continue;
                }
                if (value.isEmpty()) {
                    this.fileNames.put(key, key);
                    continue;
                }
                if (key.matches("^\\$\\{.*\\}")) {
                    this.aliases.put(key, value);
                } else {
                    Matcher aliasMatcher = aliasPattern.matcher(value);
                    StringBuilder builder = new StringBuilder();
                    if (aliasMatcher.find()) {
                        builder.append(value.substring(0, aliasMatcher.start()));
                        if (aliases.containsKey(aliasMatcher.group())) {
                            builder.append(aliases.get(aliasMatcher.group()));
                        } else {
                            builder.append(aliasMatcher.group());
                        }
                        builder.append(value.substring(aliasMatcher.end(), value.length()));
                    } else {
                        builder.append(value);
                    }
                    this.fileNames.put(key, builder.toString());
                }
            } else {
                throw new IORuntimeException("More than two elements " + fileName);
            }
        }
    }

    protected Map<String, String> getFileNames() {
        return this.fileNames;
    }

    protected void put(final String name, final IO<T> io) {
        providerMap.put(name, io);
    }

    public IO<T> get(final String name) {
        return providerMap.get(name);
    }

    public T getObject(final String name) {
        if (!providerMap.containsKey(name)) {
            throw new IORuntimeException("No key '" + name + "' found.");
        }
        try {
            return providerMap.get(name).read();
        } catch (Exception e) {
            throw new IORuntimeException("Problem reading object for '" + name + "'", e);
        }
    }
}
