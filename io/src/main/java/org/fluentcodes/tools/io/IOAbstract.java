package org.fluentcodes.tools.io;

public abstract class IOAbstract<T> implements IO<T> {
    private String fileName;

    /**
     * The empty constructor.
     */
    protected IOAbstract() {
    }

    /**
     * The constructor with the file name.
     *
     * @param fileName the file name.
     */
    protected IOAbstract(final String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }
}

