package org.fluentcodes.tools.io;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Read files from the class path as with an object mapping for each file.
 */
public abstract class IOClasspathObjectList<T> extends IOMappingAbstract<List<T>> implements IO<List<T>> {
    private final IOClasspathStringList ioStringList;

    protected IOClasspathObjectList(final String fileName, Class<?>... modelClasses) {
        super(modelClasses);
        this.ioStringList = new IOClasspathStringList(fileName);
    }

    protected IOClasspathObjectList(final String fileName, Charset encoding, Class<?>... modelClasses) {
        super(modelClasses);
        this.ioStringList = new IOClasspathStringList(fileName, encoding);
    }

    @Override
    public List<T> read() {
        return asObject(ioStringList.read());
    }

    public List<T> asObject(final List<String> in) {
        return new ArrayList<>();
    }

    @Override
    public String getFileName() {
        return ioStringList.getFileName();
    }

    @Override
    public void setFileName(final String fileName) {
        ioStringList.setFileName(fileName);
    }

}

