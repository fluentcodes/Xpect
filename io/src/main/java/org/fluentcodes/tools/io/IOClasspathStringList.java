package org.fluentcodes.tools.io;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Read files from the class path as a String list.
 */
public class IOClasspathStringList implements IO<List<String>>, IOStringInterface {
    private final IOClasspathBytesList ioBytes;
    private final Charset encoding;

    public IOClasspathStringList(final String fileName) {
        this(fileName, StandardCharsets.UTF_8);
    }

    public IOClasspathStringList(final String fileName, Charset encoding) {
        this.ioBytes = new IOClasspathBytesList(fileName);
        this.encoding = encoding;
    }

    public String asString(List<String> value) {
        return value.toString();
    }

    public List<String> asObject(String value) {
        return new ArrayList<>();
    }

    @Override
    public List<String> read() {
        List<String> stringList = new ArrayList<>();
        List<byte[]> byteList = ioBytes.read();
        for (byte[] entry : byteList) {
            stringList.add(new String(entry, encoding));
        }
        return stringList;
    }

    @Override
    public String getFileName() {
        return ioBytes.getFileName();
    }

    @Override
    public void setFileName(final String fileName) {
        ioBytes.setFileName(fileName);
    }

    @Override
    public Charset getEncoding() {
        return encoding;
    }

}

