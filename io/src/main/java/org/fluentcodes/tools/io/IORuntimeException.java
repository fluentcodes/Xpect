package org.fluentcodes.tools.io;

public class IORuntimeException extends RuntimeException {
    public IORuntimeException(final Exception e) {
        super(e);
    }

    public IORuntimeException(final String message) {
        super(message);
    }

    public IORuntimeException(final String message, final Exception e) {
        super(message, e);
    }
}
