package org.fluentcodes.tools.io;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Read files from the class path as with an object mapping for each file.
 */
public class IOClasspathJacksonFlatList<T> {
    private final IOClasspathObjectList<List<T>> ioList;
    public IOClasspathJacksonFlatList(final String fileName, Class<?> modelClass ) {
        this(fileName, StandardCharsets.UTF_8, modelClass);
    }

    public IOClasspathJacksonFlatList(final String fileName, Charset encoding, Class<?> modelClass ) {
        Class<?>[] listClass = new Class[]{List.class, modelClass};
        ioList = new IOClasspathJacksonList<>(fileName, encoding, listClass);
    }

    public List<T> read() {
        List<T> flat = new ArrayList<>();
        List<List<T>> list = ioList.read();
        for (List<T> cpEntry: list) {
            flat.addAll(cpEntry);
        }
        return flat;
    }
}

