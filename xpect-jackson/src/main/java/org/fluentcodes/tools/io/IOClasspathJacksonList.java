package org.fluentcodes.tools.io;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Read files from the class path as with an object mapping for each file.
 */
public class IOClasspathJacksonList<T> extends IOClasspathObjectList<T> {

    public IOClasspathJacksonList(final String fileName, Class<?>... modelClasses) {
        super(fileName, modelClasses);
    }

    public IOClasspathJacksonList(final String fileName, Charset encoding, Class<?>... modelClasses) {
        super(fileName, encoding, modelClasses);
    }

    @Override
    public List<T> asObject(final List<String> in) {
        List<T> objectList = new ArrayList<>();
        for (String entry : in) {
            objectList.add(new IOJackson<T>(getMappingClasses()).asObject(entry));
        }
        return objectList;
    }

    @Override
    public List<T> asObject(final String in) {
        List<T> objectList = new ArrayList<>();
        objectList.add(new IOJackson<T>(getMappingClasses()).asObject(in));
        return objectList;
    }

    @Override
    public String asString(final List<T> in) {
        StringBuilder listString = new StringBuilder("[");
        for (int i = 0; i < in.size(); i++) {
            listString.append(new IOJackson<T>(getMappingClasses()).asString(in.get(i)));
            if (i < in.size() - 1) {
                listString.append(",");
            }
        }
        listString.append("]");
        return listString.toString();
    }
}

