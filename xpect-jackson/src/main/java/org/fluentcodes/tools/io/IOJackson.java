package org.fluentcodes.tools.io;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.joda.time.LocalDate;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * An IO mapping with help of Jackson.
 * @param <T>
 */
public class IOJackson<T> extends IOMappingObject<T> {
    static final ObjectMapper mapper = init();

    public IOJackson(Class<?> ... classes) {
        super(classes);
    }

    public IOJackson(final String fileName, Class<?> ... classes) {
        super(fileName, classes);
    }

    private static ObjectMapper init() {
        LocalDateSerializer localDateSerializer = new  LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDateTimeSerializer localDateTimeSerializer = new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        SimpleModule simpleModule = new SimpleModule("SimpleModule");
        simpleModule.addSerializer(localDateSerializer);
        simpleModule.addSerializer(localDateTimeSerializer);
        simpleModule.addSerializer(LocalDate.class, new JsonSerializer<LocalDate>() {
            @Override
            public void serialize(LocalDate date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                jsonGenerator.writeString(date.toString());
            }
        });
        ObjectMapper mapper = new ObjectMapper();
        //https://github.com/FasterXML/jackson-core/wiki/JsonParser-Features
        mapper.enable(JsonParser.Feature.ALLOW_COMMENTS);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        //https://github.com/FasterXML/jackson-databind/wiki/Mapper-Features
        //https://fasterxml.github.io/jackson-core/javadoc/1.9/org/codehaus/jackson/map/SerializationConfig.Feature.html
        mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
        //https://github.com/FasterXML/jackson-databind/wiki/Deserialization-Features
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);

        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(simpleModule);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    @Override
    public String asString(T object) {
        if (object == null) {
            throw new IORuntimeException("Null object for serialiazation!");
        }
        try {
            return mapper
                    .writerWithDefaultPrettyPrinter()
                    .withoutFeatures(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                    .writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new IORuntimeException(e);
        }
    }


    @Override
    public T asObject(final String asString) {
        if (asString == null || asString.isEmpty()) {
            throw new IORuntimeException("Null string could not convert");
        }
        if (getMappingClass() == List.class && getMappingClasses().length>1) {
            return asObjectList(asString);
        }
        else if (getMappingClass() == Map.class && getMappingClasses().length > 2) {
            return asObjectMap(asString);
        }
        try {
            return (T) mapper.readValue(asString, getMappingClass());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    private T asObjectList(final String asString) {
        JavaType itemType = mapper.getTypeFactory().constructCollectionType(List.class, getMappingClasses()[1]);
        try {
            return mapper.readValue(asString, itemType);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    private T asObjectMap(final String asString) {
        JavaType itemType = mapper.getTypeFactory().constructMapType(Map.class, getMappingClasses()[1], getMappingClasses()[2]);
        try {
            return mapper.readValue(asString, itemType);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }
}
