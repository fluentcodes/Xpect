package org.fluentcodes.tools.xpect;

import org.fluentcodes.tools.io.IOJackson;

/**
 * Will compare the persisted file with an object as String via JUnit4.
 */
public abstract class XpectJackson extends XpectAbstract{
    public XpectJackson(Class annotationClass, Class<?>... classes) {
        super(new IOJackson<>(classes), "json", annotationClass);
    }
}
