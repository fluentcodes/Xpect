package org.fluentcodes.tools.xpect;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.ihe.gematik.fdv.model.DocumentWithMetadata;
import org.fluentcodes.tools.testobjects.ForTestClass;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Some examples are from generated code delivered by a maven artifact from a project last year with gematik
 */
public class XpectJacksonJunit4Test {
    public static final String XPECT_DIR = "src/test/resources/XpectJackson/";

    @Test
    public void testHashMap() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        XpectJacksonJunit4.assertStatic(map);
    }

    @Test
    public void testHashMapAsObject() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        XpectJacksonJunit4.assertStatic(map);
    }

    @Test
    public void testForTestClassCompareAsString() {
        XpectJacksonJunit4.assertStatic(ForTestClass.of1());
    }

    @Test
    public void testForTestClassAsObject() {
        XpectJacksonJunit4.assertStatic(ForTestClass.of1());
    }

    @Test
    public void testDocumentTTWithClassCode() {
        DocumentMetadata documentTT = new DocumentMetadata();
        documentTT.setClassCode("classCode");
        XpectJacksonJunit4.assertStatic(documentTT);
    }

}
