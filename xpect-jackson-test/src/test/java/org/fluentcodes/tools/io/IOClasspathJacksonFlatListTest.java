package org.fluentcodes.tools.io;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.tools.xpect.XpectJacksonJunit4;
import org.junit.Test;

import java.util.List;

public class IOClasspathJacksonFlatListTest {

    @Test
    public void documentListXpected() {
        IOClasspathJacksonFlatList<DocumentMetadata> io =
                new IOClasspathJacksonFlatList<>("documentList.json", DocumentMetadata.class);
        List<DocumentMetadata> documents = io.read();

        XpectJacksonJunit4.assertStatic(documents);
    }
}
