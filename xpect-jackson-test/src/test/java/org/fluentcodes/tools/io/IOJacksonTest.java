package org.fluentcodes.tools.io;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.tools.testobjects.DocumentMetaDataTestBuilder;
import org.fluentcodes.tools.xpect.XpectJacksonJunit4;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.fluentcodes.tools.testobjects.DocumentMetaDataTestBuilder;

public class IOJacksonTest {
    @Test
    public void documentXpected() {
        DocumentMetadata document = DocumentMetaDataTestBuilder.of1();

        IOJackson<DocumentMetadata> ioJackson = new IOJackson<>(DocumentMetadata.class);

        String serialized = ioJackson
                .asString(document);

        DocumentMetadata documentFromJson = ioJackson
                .asObject(serialized);

        XpectJacksonJunit4.assertStatic(documentFromJson);
    }

    @Test
    public void documentListXpected() {
        List<DocumentMetadata> documents = new ArrayList<>();
        documents.add(DocumentMetaDataTestBuilder.of1());
        documents.add(DocumentMetaDataTestBuilder.of2());

        IOJackson<List<DocumentMetadata>> ioJackson = new IOJackson<>(List.class, DocumentMetadata.class);

        String serialized = ioJackson
                .asString(documents);
        List<DocumentMetadata> documentsFromJson = ioJackson
                .asObject(serialized);
        Assert.assertEquals(documents.size(), documentsFromJson.size());
        XpectJacksonJunit4.assertStatic(documentsFromJson);
    }

    @Test
    public void documentMapXpected() {
        Map<String, DocumentMetadata> documents = new LinkedHashMap<>();
        documents.put("key1", DocumentMetaDataTestBuilder.of1());
        documents.put("key2", DocumentMetaDataTestBuilder.of2());

        IOJackson<Map<String, DocumentMetadata>> ioJackson = new IOJackson<>(Map.class, String.class, DocumentMetadata.class);

        String serialized = ioJackson
                .asString(documents);
        Map<String, DocumentMetadata> documentsFromJson = ioJackson
                .asObject(serialized);
        Assert.assertEquals(documents.size(), documentsFromJson.size());
        XpectJacksonJunit4.assertStatic(documentsFromJson);
    }

    @Test
    public void testReadList() {
        List list = new IOJackson<List>("testList.json", List.class)
                .read();
        XpectJacksonJunit4.assertStatic(list);
    }

    @Test
    public void testReadMap() {
        Map map = new IOJackson<Map>("testMap.json", Map.class).read();
        XpectJacksonJunit4.assertStatic(map);
    }

    @Test(expected = IORuntimeException.class)
    public void fileNotExist_throwsIORuntimeException() {
        new IOJackson<Map>("noMap.json", Map.class).read();
    }

    @Test(expected = IORuntimeException.class)
    public void testListJson_MapClass_throwsIORuntimeException() {
        new IOJackson<Map>("testList.json", Map.class).read();
    }

    @Test(expected = IORuntimeException.class)
    public void testMapJson_ListClass_throwsIORuntimeException() {
        new IOJackson<Map>("testMap.json", List.class).read();
    }

    @Test(expected = IORuntimeException.class)
    public void empty_throwsIORuntimeException() {
        new IOJackson<Map>().read();
    }

}
