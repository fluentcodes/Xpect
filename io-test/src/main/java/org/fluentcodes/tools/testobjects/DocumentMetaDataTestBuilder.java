package org.fluentcodes.tools.testobjects;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;

import java.time.Instant;

public class DocumentMetaDataTestBuilder {
    private Instant createTime;
    private String uniqueId;
    private String classCode;

    private DocumentMetaDataTestBuilder() {
    }

    public static final DocumentMetadata of1(){
        return new DocumentMetaDataTestBuilder()
                .setClassCode("classcode1")
                .setCreateTime(Instant.MIN)
                .setUniqueId("id1")
                .build();
    }

    public static final DocumentMetadata of2(){
        return new DocumentMetaDataTestBuilder()
                .setClassCode("classcode2")
                .setCreateTime(Instant.MAX)
                .setUniqueId("id2")
                .build();
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public DocumentMetaDataTestBuilder setCreateTime(Instant createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public DocumentMetaDataTestBuilder setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
        return this;
    }

    public String getClassCode() {
        return classCode;
    }

    public DocumentMetaDataTestBuilder setClassCode(String classCode) {
        this.classCode = classCode;
        return this;
    }

    public DocumentMetadata build() {
        DocumentMetadata document = new DocumentMetadata();
        document.setCreationTime(this.createTime);
        document.setClassCode(classCode);
        document.setUniqueId(uniqueId);
        return document;
    }

}
