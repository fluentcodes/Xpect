package org.fluentcodes.tools.testobjects;

public class Contact {
    private String type;
    private int number;
    // getters and setters

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
