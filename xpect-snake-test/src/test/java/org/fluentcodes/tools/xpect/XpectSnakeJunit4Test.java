package org.fluentcodes.tools.xpect;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.ihe.gematik.fdv.model.DocumentWithMetadata;
import org.fluentcodes.tools.testobjects.ForTestClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Some examples are from generated code delivered by a maven artifact from a project last year with gematik
 */
public class XpectSnakeJunit4Test {
    @Test
    public void testHashMap() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        XpectSnakeJunit4.assertStatic(map);
    }

    @Test
    public void testHashMapAsObject() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        XpectSnakeJunit4.assertStatic(map);
    }

    @Test
    public void testForTestClass() {
        XpectSnakeJunit4.assertStatic(ForTestClass.of1());
    }

    @Ignore("Problem persisting this object")
    @Test
    public void testForTestClassAsObject() {
        XpectSnakeJunit4.assertStatic(ForTestClass.of1());
    }

    @Test
    public void testDocumentTTWithClassCode() {
        DocumentMetadata documentTT = new DocumentMetadata();
        documentTT.setClassCode("classCode");
        XpectSnakeJunit4.assertStatic(documentTT);
    }
}
