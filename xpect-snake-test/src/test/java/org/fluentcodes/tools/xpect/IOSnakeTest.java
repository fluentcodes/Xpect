package org.fluentcodes.tools.xpect;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.ihe.gematik.fdv.model.DocumentWithMetadata;
import org.fluentcodes.tools.io.IOSnake;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class IOSnakeTest {

    @Test
    public void testHashMap() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        XpectSnakeJunit4.assertStatic(map);
    }

    @Test
    public void testAsObjectList() {
        List<DocumentMetadata> documents = new ArrayList<>();
        DocumentMetadata document = new DocumentMetadata();
        document.setUniqueId(UUID.randomUUID().toString());
        documents.add(document);
        DocumentMetadata document2 = new DocumentMetadata();
        document2.setUniqueId(UUID.randomUUID().toString());
        documents.add(document2);

        IOSnake<List<DocumentMetadata>> ioYaml = new IOSnake<>(
                "snake", List.class, String.class, DocumentMetadata.class);

        String serialized = ioYaml.asString(documents);


        List<DocumentMetadata> documentsFromJson = ioYaml
                .asObject(serialized);
        Assert.assertEquals(documents.size(), documentsFromJson.size());
    }

    @Test
    public void testAsObjectMap() {
        Map<String, DocumentMetadata> documents = new LinkedHashMap<>();
        DocumentMetadata document = new DocumentMetadata();
        document.setUniqueId(UUID.randomUUID().toString());
        documents.put(document.getUniqueId(), document);
        DocumentMetadata document2 = new DocumentMetadata();
        document2.setUniqueId(UUID.randomUUID().toString());
        documents.put(document2.getUniqueId(), document2);

        IOSnake<Map<String, DocumentMetadata>> ioYaml = new IOSnake<>(
                "snake", Map.class, String.class, DocumentMetadata.class);

        String serialized = ioYaml.asString(documents);
        Map<String, DocumentMetadata> documentsFromJson = ioYaml.asObject(serialized);
        Assert.assertEquals(documents.size(), ((LinkedHashMap)documentsFromJson).size());
    }


}
