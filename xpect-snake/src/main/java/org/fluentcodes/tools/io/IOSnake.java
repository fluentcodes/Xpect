package org.fluentcodes.tools.io;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class IOSnake<T> extends IOMappingObject<T> {
    public IOSnake(final String fileName, final Class<?>... classes) {
        super(fileName, classes);
    }
    public IOSnake(final Class<?>... classes) {
        super(classes);
    }

    private Yaml createYaml() {
        if (hasMappingClasses()) {
            try {
                return new Yaml(new Constructor(getMappingClass()));
            } catch (Exception e) {
                throw new IORuntimeException(e);
            }
        } else {
            return new Yaml();
        }
    }

    @Override
    public String asString(T object) {
        if (object == null) {
            throw new IORuntimeException("Null object for serialiazation!");
        }
        return createYaml().dump(object);
    }

    @Override
    public T asObject(final String asString) {
        return (T) createYaml().load(asString);
    }
}
