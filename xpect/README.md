[fluentcodes.com](https://fluentcodes.com) - [Xpect](../README.md)
# Xpect

## IO
This module contains some basic implementations of the interface IO with some obvious methods:

    String asString(T object);
    T asObject(String object);

    T read();
    File write(T object);

Some of the provided methods are for type based serialization/deserializion: 

    Class getMappingClass();
    IO<T> setMappingClasses(List<Class> mappingClasses);
    IO<T> setMappingClass(Class mappingClass);
    IO<T> setMappingClass(T object);

