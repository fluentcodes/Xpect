package org.fluentcodes.tools.xpect;

import org.fluentcodes.tools.io.IO;
import org.fluentcodes.tools.io.IOProperties;
import org.fluentcodes.tools.io.IORuntimeException;
import org.fluentcodes.tools.io.IOString;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * Will provide stored io from the constructor.
 */
public abstract class XpectAbstract implements Xpect {
    private static final String DEFAULT_XPECT_DIRECTORY = "src/test/resources/Xpect/";
    private static final String XPECT_DIRECTORY = loadXpectDirectory();
    private final IO<Object> io;
    private final String fileEnding;
    private final Class annotationClass;
    protected XpectAbstract(IO<Object> io, final String fileEnding, Class annotationClass) {
        this.io = io;
        this.fileEnding = fileEnding;
        this.annotationClass = annotationClass;
        io.setFileName(determinePersistenceFile());
    }

    protected IO<Object> getIo() {
        return io;
    }

    protected String load(Object toCompare) {
        String persisted = null;
        try {
            persisted = new IOString(io.getFileName()).read();
        }
        catch (IORuntimeException e) {
            persisted = io.asString(toCompare);
            new IOString(io.getFileName()).write(persisted);
        }
        return CR_PATTERN.matcher(persisted).replaceAll("\\r");
    }


    private static final String loadXpectDirectory() {
        final String XPECT_DIR = "xpect.dir";
        if (System.getProperty(XPECT_DIR) != null && !System.getProperty(XPECT_DIR).isEmpty()) {
            return System.getProperty(XPECT_DIR);
        } else {
            try {
                Properties properties = new IOProperties().read();
                return properties.getProperty(XPECT_DIR, DEFAULT_XPECT_DIRECTORY);
            } catch (IORuntimeException e)  {
                return DEFAULT_XPECT_DIRECTORY;
            }
        }
    }

    /**
     * Determine the persistence file for comparision.
     * xpectDirectory/testClassName/testMethod.fileEnding
     *
     * @return fileName to persist and compare
     */
    String determinePersistenceFile() {
        return determinePersistenceFile(null);
    }

    /**
     * Determine the persistence file for comparision.
     * xpectDirectory/testClassName/testMethod_mappingClass.fileEnding
     *
     * @param mappingClass mappingClass will be used for persistence file name
     * @return  fileName to persist and compare
     */
    String determinePersistenceFile(final Class<?> mappingClass) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (int i = 2; i < elements.length; i++) {
            String className = elements[i].getClassName();
            String methodName = elements[i].getMethodName();

            Class<?> testClass;
            try {
                testClass = Class.forName(className);
                if (!isTestClass(testClass, methodName)) {
                    continue;
                }
            } catch (Exception e) {
                throw new IORuntimeException(e);
            }

            StringBuilder fileName = new StringBuilder(XPECT_DIRECTORY);
            fileName.append(testClass.getSimpleName());
            fileName.append("/");
            fileName.append(methodName);
            if (mappingClass != null) {
                fileName.append("_");
                fileName.append(mappingClass.getSimpleName());
            }
            fileName.append(".");
            fileName.append(this.fileEnding);

            final String fileNameString = fileName.toString();
            IO.createDirs(new File(fileNameString).getParentFile());
            return fileNameString;
        }
        throw new IORuntimeException("Could not getValues filename for Test! ");
    }

    private boolean isTestClass(Class<?> testClass, String methodName) {
        try {
            Method method = testClass.getMethod(methodName);
            return method.getAnnotation(annotationClass) != null;
        } catch (Exception e) {
            return false;
        }
    }
}
