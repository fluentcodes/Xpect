package org.fluentcodes.tools.xpect;

import org.fluentcodes.tools.io.IO;
import org.fluentcodes.tools.io.IOString;

/**
 * Will compare the persisted file with an object as String via JUnit4.
 */
public abstract class XpectString extends XpectAbstract implements Xpect {
    public XpectString(Class annotationClass) {
        super((IO) new IOString(), "txt", annotationClass);
    }

}
