package org.fluentcodes.tools.xpect;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Will compare the persisted file with an object as xml String via JUnit4.
 */
public class XpectJaxbJunit4 extends XpectJaxb {
    public XpectJaxbJunit4(Class<?> mapClass) {
        super(Test.class, mapClass);
    }

    public XpectJaxbJunit4(Object value) {
        super(Test.class, value.getClass());
    }

    public static File assertStatic(final Object toCompare) {
        XpectJaxbJunit4 xpect = new XpectJaxbJunit4(toCompare);
        return xpect.assertEquals(toCompare);
    }

    @Override
    public File assertEquals(final Object toCompare) {
        Assert.assertEquals(load(toCompare), getIo().asString(toCompare));
        return new File(getIo().getFileName());
    }


}
