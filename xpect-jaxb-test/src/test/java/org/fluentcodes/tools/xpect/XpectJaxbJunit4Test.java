package org.fluentcodes.tools.xpect;

import oasis.names.tc.ebxml_regrep.xsd.rim._3.ExtrinsicObjectType;
import org.hl7.v3.II;
import org.junit.Test;

/**
 * @Author wdi
 */
public class XpectJaxbJunit4Test {
    @Test
    public void testExtrinsicObject() {
        ExtrinsicObjectType document = new ExtrinsicObjectType();
        document.setId("ID");
        XpectJaxbJunit4.assertStatic(document);
    }

    @Test
    public void testHl7II() {
        II authority = new II();
        authority.setExtension("test");
        XpectJaxbJunit4.assertStatic(authority);
    }
}
