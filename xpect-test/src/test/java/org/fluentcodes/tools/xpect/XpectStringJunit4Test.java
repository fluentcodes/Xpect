package org.fluentcodes.tools.xpect;
import org.junit.Assert;
import org.junit.Test;
import java.io.File;

/**
 * @Author wdi
 */
public class XpectStringJunit4Test {

    @Test
    public void testCompare() {
        File file = new XpectStringJunit4().assertEquals("test");
        Assert.assertEquals("src/test/resources/Xpect/XpectStringJunit4Test/testCompare.txt", file.getPath());
    }

}
