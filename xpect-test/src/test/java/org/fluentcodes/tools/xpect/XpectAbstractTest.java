package org.fluentcodes.tools.xpect;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XpectAbstractTest {
    @Test
    public void determinePersistenceFileName() {
        assertEquals("src/test/resources/Xpect/XpectAbstractTest/determinePersistenceFileName.txt", new XpectStringJunit4().determinePersistenceFile());
    }

    @Test
    public void determinePersistenceFileNameWithClass() {
        assertEquals("src/test/resources/Xpect/XpectAbstractTest/determinePersistenceFileNameWithClass_XpectAbstractTest.txt", new XpectStringJunit4().determinePersistenceFile(XpectAbstractTest.class));
    }
}
