package org.fluentcodes.tools.xpect;

import org.fluentcodes.tools.io.IOJaxb;

public abstract class XpectJaxb extends XpectAbstract {
    public XpectJaxb(Class annotationClass, Class<?> mapClass) {
        super(new IOJaxb<Object>(mapClass), "xml", annotationClass);
    }
}
