package org.fluentcodes.tools.xpect;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.tools.testobjects.ForTestClass;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Some examples are from generated code delivered by a maven artifact from a project last year with gematik
 */
public class XpectGsonJunit4Test {
    @Test
    public void testHashMap() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        XpectGsonJunit4.assertStatic(map);
    }

    @Test
    public void testHashMapAsObject() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        XpectGsonJunit4.assertStatic(map);

    }

    @Test
    public void testForTestClass() {
        XpectGsonJunit4.assertStatic(ForTestClass.of1());
    }

    @Test
    public void testForTestClassAsObject() {
        XpectGsonJunit4.assertStatic(ForTestClass.of1());
    }

    @Test
    public void testDocumentTTWithClassCode() {
        DocumentMetadata documentTT = new DocumentMetadata();
        documentTT.setClassCode("classCode");
        XpectGsonJunit4.assertStatic(documentTT);
    }

    @Test
    public void testDocumentTTWithClassCodeChanged_AssertDifferent() {
        DocumentMetadata documentTT = new DocumentMetadata();
        documentTT.setClassCode("classCode2");
        XpectGsonJunit4.assertStatic(documentTT);  // inialize file if not existing
    }

}
