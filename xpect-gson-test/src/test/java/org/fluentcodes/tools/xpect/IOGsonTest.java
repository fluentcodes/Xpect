package org.fluentcodes.tools.xpect;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.tools.io.IOGson;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class IOGsonTest {

    @Test
    public void testAsObjectList() {
        List<DocumentMetadata> documents = new ArrayList();
        DocumentMetadata document = new DocumentMetadata();
        document.setUniqueId(UUID.randomUUID().toString());
        documents.add(document);
        DocumentMetadata document2 = new DocumentMetadata();
        document2.setUniqueId(UUID.randomUUID().toString());
        documents.add(document2);

        IOGson<List<DocumentMetadata>> ioGson = new IOGson<>(List.class);

        String serialized = ioGson
                .asString(documents);

        List<DocumentMetadata> documentsFromJson = ioGson
                .asObject(serialized);
        Assert.assertEquals(documents.size(), documentsFromJson.size());
    }

    @Test
    public void testAsObjectMap() {
        Map<String, DocumentMetadata> documents = new LinkedHashMap<>();
        DocumentMetadata document = new DocumentMetadata();
        document.setUniqueId(UUID.randomUUID().toString());
        documents.put(document.getUniqueId(), document);
        DocumentMetadata document2 = new DocumentMetadata();
        document2.setUniqueId(UUID.randomUUID().toString());
        documents.put(document.getUniqueId(), document2);

        IOGson<Map<String, DocumentMetadata>> ioGson = new IOGson<>(Map.class);
        String serialized = ioGson
                .asString(documents);
        Map<String,DocumentMetadata> documentsFromJson = ioGson
                .asObject(serialized);
        Assert.assertEquals(documents.size(), documentsFromJson.size());
    }


}
